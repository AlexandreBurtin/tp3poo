﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp3.Cons
{
    class MaClasse
    {
        public MaClasse()
        {
            Console.WriteLine("Constructeur");
        }
        ~MaClasse()
        {
            Console.WriteLine("Destructeur");
        }
    }
}
