﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp3.Cons
{
    class Colors
    {
        private byte m_R;
        private byte m_V;
        private byte m_B;
        private byte m_MaxIntensity;
        private static Colors m_Black = new Colors(0, 0, 0);
        private static Colors m_White = new Colors(255, 255, 255);
        private static Colors m_Blue = new Colors(0, 0, 255);
        public Colors(byte p_R, byte p_V, byte p_B)
        {
            m_R = p_R;
            m_V = p_V;
            m_B = p_B;
        }
    
    }
}
