﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp3.Cons
{
     abstract class Vehicule
    {
        private String m_type;
        private byte m_nb_passenger;
        private int m_nb_capacity;
        public Vehicule(String p_type, byte p_nb_passenger, int p_nb_capacity)
        {
            m_type = p_type;
            m_nb_passenger = p_nb_passenger;
            m_nb_capacity = p_nb_capacity;
        }

        public Vehicule(String p_type, byte p_nb_passenger) : this(p_type, p_nb_passenger, 0) { }

        public Vehicule(String p_type) : this(p_type, 1, 0) { }

        public abstract String SeDeplacer();

        public virtual String GetInfosTechniques()
        {
            return String.Format("Type : {0}\nNbPassenger : {1}\nNbCapacity : {2}\n", m_type, m_nb_passenger, m_nb_capacity);
        }

    }

}
